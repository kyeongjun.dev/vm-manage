from django.shortcuts import render
from django.http import HttpResponse
from .models import Server
import subprocess

# func
def check_server_status(root_passwd, server_ip, ssh_port):
    # 주의사항 : 맨 앞에 띄어쓰기 있어야함
    cmd = " systemctl is-active sshd.service"
    try:         
        out = subprocess.check_output(["ssh", "-o", "ConnectTimeout=1", "root@" + server_ip, "-p", ssh_port, cmd], encoding='utf-8')
    except:
        out = 'dead'
    return out

def create_server_using_vagrant(root_passwd, server_ip, ssh_port):
    #cmd = "sshpass -p 'dclabroot311!' scp -P 6151 -o StrictHostKeyChecking=no /home/ubuntu/Projects/vm-manage/Vagrantfile root@113.198.137.97:/root/Vagrantfile"
    out = subprocess.check_output(["sshpass -p " + root_passwd + " scp -P " + ssh_port + " -o StrictHostKeyChecking=no /home/ubuntu/Projects/vm-manage/Vagrantfile root@" + server_ip + ":/root/Vagrantfile"], shell=True, encoding='utf-8')

    cmd = " vagrant up"
    out = subprocess.check_output(["sshpass -p " + root_passwd + " ssh root@" + server_ip + " -p " + ssh_port + cmd], shell=True, encoding='utf-8')
# Create your views here.
def index(request):
    servers = Server.objects.all()
    for server in servers:
        try:
            out = check_server_status(server.server_root_passwd, server.server_ip, server.ssh_port)
        except:
            out = 'dead'
        server.status = out
        server.save()
    context = {'servers': servers}
    return render(request, 'serverapp/server_list.html', context)

def detail(request, server_id):
    server = Server.objects.get(id=server_id)
    context = {'server': server}
    return render(request, 'serverapp/server_detail.html', context)

def create(request, server_id):
    server = Server.objects.get(id=server_id)
    create_server_using_vagrant(server.server_root_passwd, server.server_ip, server.ssh_port)

    return HttpResponse(f'create vm at: {server.server_ip}')

def delete(request, server_id):
    server = Server.objects.get(id=server_id)
    return HttpResponse(f'delete vm at: {server.server_ip}')

def reset(request, server_id):
    server = Server.objects.get(id=server_id)
    return HttpResponse(f'reset vm at: {server.server_ip}')
