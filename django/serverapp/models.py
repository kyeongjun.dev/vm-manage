from django.db import models

# Create your models here.
class Server(models.Model):
    server_ip = models.CharField(max_length=20)
    ssh_port = models.CharField(max_length=10, default='22')
    server_root_passwd = models.CharField(max_length=20)
    status = models.CharField(max_length=10, default='dead')
    def __str__(self):
        return self.server_ip
