from django.urls import path
from . import views

app_name = 'server'
urlpatterns = [
    path('', views.index),
    path('<int:server_id>/', views.detail, name='detail'),
    path('<int:server_id>/create', views.create),
    path('<int:server_id>/delete', views.delete),
    path('<int:server_id>/reset', views.delete),
]