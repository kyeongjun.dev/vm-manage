from django.urls import path
from . import views

app_name = 'test'

urlpatterns = [
    path('', views.index),
    path('<int:test_id>/', views.detail, name='detail'),
]
