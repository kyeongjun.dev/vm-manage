from django.db import models

# Create your models here.

class Test(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return self.name

class Common(models.Model):
    name = models.CharField(max_length=100)
    answer = models.CharField(max_length=100)
    command = models.TextField()
    test = models.ForeignKey(Test, null=True, on_delete=models.CASCADE)
    class Meta():
        abstract = True
    def __str__(self):
        return self.name

class QuestionTwo(Common):
    pass

class QuestionThree(Common):
    attribute_1 = models.CharField(max_length=100)

class QuestionFour(Common):
    attribute_1 = models.CharField(max_length=100)
    attribute_2 = models.CharField(max_length=100)