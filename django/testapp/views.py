from django.shortcuts import render
from django.http import HttpResponse
from .models import Test
# Create your views here.
def index(request):
    tests = Test.objects.all()
    context = {'tests': tests}
    return render(request, 'testapp/test_list.html', context)

def detail(request, test_id):
    test = Test.objects.get(id=test_id)
    context = {'test': test}
    return render(request, 'testapp/test_detail.html', context)
