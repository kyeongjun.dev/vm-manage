from django.contrib import admin
from .models import QuestionFour, QuestionThree, QuestionTwo, Test
# Register your models here.
admin.site.register(Test)
admin.site.register(QuestionTwo)
admin.site.register(QuestionThree)
admin.site.register(QuestionFour)