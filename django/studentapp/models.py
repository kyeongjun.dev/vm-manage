from django.db import models
from testapp.models import Test
from serverapp.models import Server
# Create your models here.
class Student(models.Model):
    student_id = models.CharField(max_length=10, unique=True)
    test = models.ForeignKey(Test, null=True, on_delete=models.CASCADE)
    server = models.ForeignKey(Server, null=True, on_delete=models.SET_NULL)
    def __str__(self):
        return self.student_id